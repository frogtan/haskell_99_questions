--(*) Find the last element of a list.
--
--(Note that the Lisp transcription of this problem is incorrect.)
--
--Example in Haskell:
--
--Prelude> myLast [1,2,3,4]
--4
--Prelude> myLast ['x','y','z']
--'z'

myLast :: [a] -> a
myLast = last

myLast' :: [a] -> a
myLast' xs = head $ reverse xs

myLast'' :: [a] -> a
myLast'' = foldl1 (\_ x -> x)

myLast''' :: [a] -> a
myLast''' = foldr1 (\x acc -> acc)

myLast'''' :: [a] -> a
myLast'''' = foldr1 (const id)
